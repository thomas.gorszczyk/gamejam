using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DalleTrigger : MonoBehaviour {
    public TriggerAbstract[] triggeredObjects;

    public bool isStayPressed = false;

    void OnTriggerEnter (Collider other)
    {
        triggerObjects();
    }

    void OnTriggerExit(Collider other)
    {
        if (isStayPressed)
        {
            triggerObjects();
        }
    }

    private void triggerObjects()
    {
        if (triggeredObjects.Length > 0)
        {
            for (int i = 0; i < triggeredObjects.Length; i++)
            {
                triggeredObjects[i].Trigger();
            }
        }
    }
}
