using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerAbstract : MonoBehaviour
{
    public bool isTriggered = false;

    public abstract void Trigger();
}
