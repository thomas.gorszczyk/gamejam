using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    public LayerMask mask;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }


    private void FixedUpdate()
    {
        if (GameManager.instance.isDay())
        {
            if (Physics.Raycast(transform.position, transform.forward, out var hit, 2f, mask))
            {

                var obj = hit.collider.gameObject;
                if (obj.name == "Trapped_Fireplace_Model")
                {
                    gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    StartCoroutine(burnToDeath());
                }

            }
        }   
    }
    IEnumerator burnToDeath()
    {
        yield return new WaitForSeconds(4);
        GameManager.instance.gameState = GameManager.GameState.gameOver;
    }

    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 2f;
        Gizmos.DrawRay(transform.position, direction);
    }
}
