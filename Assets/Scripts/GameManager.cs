using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StarterAssets;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject playerCapsule;

    public GameObject createPotion;

    public GameObject winningText;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }

        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Start()
    {
        playerCapsule = GameObject.Find("PlayerCapsule");
    }


    public enum GameState { nothing, dayTransition , playing , potionUI , gameOver , victory, readingBook };
    public GameState gameState = GameState.dayTransition;

    [Serializable]
    public struct TimeSteps
    {
        public DayOrNight dayOrNight;
        public float time;
        public string transitionText;
    }

    public List<TimeSteps> timeStep =  new List<TimeSteps>(); 

    public enum DayOrNight { day, night};
    public float timeSinceLastSteps = 0f;

    public int dayOrNightStepNumber = 0;


    [Header("Transition")]
    public bool transitionIsDisplay;
    public float transitionDuration = 5f;
    public float currentTransitionTime = 0f;
    public GameObject transitionText;

    [Header("Inventory")]
    public List<itemToMakePotion> itemsToMakePotion;


    /*[Header("Outline")]
    public Color outlineColor = Color.yellow;
    public float outlineWidth = 5f;*/

    private void Update()
    {
        switch (gameState)
        {
            case GameState.playing:
                //Action pendant le jeu
                if (dayOrNightStepNumber < timeStep.Count - 1 && timeSinceLastSteps > timeStep[dayOrNightStepNumber].time)
                {
                    dayOrNightStepNumber++;
                    timeSinceLastSteps = 0f;

                    gameState = GameState.dayTransition;
                    
             
                } else if(dayOrNightStepNumber == timeStep.Count - 1 && timeSinceLastSteps > timeStep[dayOrNightStepNumber].time)
                {
                    gameState = GameState.gameOver;
                } 

                timeSinceLastSteps += Time.deltaTime;

                break;
            case GameState.dayTransition:
                //J'affiche le message de transition
                if (!transitionIsDisplay)
                {
                    transitionIsDisplay = true;
                    transitionText.SetActive(true);
                    transitionText.GetComponent<Text>().text = timeStep[dayOrNightStepNumber].transitionText;

                } 
                else if (transitionIsDisplay && currentTransitionTime >= transitionDuration)
                {
                    gameState = GameState.playing;

                    timeSinceLastSteps = 0f;
                    currentTransitionTime = 0f;
                    transitionIsDisplay = false;
                    transitionText.SetActive(false);
                }

                currentTransitionTime += Time.deltaTime;

                break;
            case GameState.gameOver:

                if (!transitionText.activeSelf)
                {
                    transitionIsDisplay = true;
                    transitionText.SetActive(true);
                    transitionText.GetComponent<Text>().text = "MDR T MORT";
                }
                break;
            case GameState.victory:
                

                if (!winningText.activeSelf)
                {
                    createPotion.gameObject.SetActive(false);
                    winningText.SetActive(true);
                }

                break;

            default:
                break;        
        }


        if (isDay() && !playerCapsule.GetComponent<FirstPersonController>().human)
        {
            playerCapsule.GetComponent<FirstPersonController>().human = true;
            // playerCapsule.GetComponent<Rigidbody>().mass = 1;
        }
        else if (!isDay() && playerCapsule.GetComponent<FirstPersonController>().human)
        {
            playerCapsule.GetComponent<FirstPersonController>().human = false;
           // playerCapsule.GetComponent<Rigidbody>().mass = 130;
        }


    }

    public void displayTransition()
    {

    }

    public float timeOfTheDay()
    {
        return timeSinceLastSteps * 24 / timeStep[dayOrNightStepNumber].time;
    }

    public bool isDay()
    {
        return timeStep[dayOrNightStepNumber].dayOrNight == DayOrNight.day;
    }
}
