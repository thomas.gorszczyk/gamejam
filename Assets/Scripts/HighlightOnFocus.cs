using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HighlightOnFocus : MonoBehaviour
{

    public static string selectedObject;
    public string internalObject;
    public RaycastHit theObject;
    public bool triggered = false;
    public GameObject lastTriggered;
    public GameObject player;
    public GameObject playerHold;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out theObject))
        {
            internalObject = selectedObject = theObject.transform.gameObject.name;

            if (theObject.transform.tag == "selectable" && theObject.distance <= 3f && theObject.transform.gameObject.GetComponent<Outline>())
            {

                triggered = true;
                lastTriggered = theObject.transform.gameObject;


                //test if hint as been discorered
                if (theObject.transform.gameObject.GetComponentInChildren<Canvas>() != null && player.GetComponent<FirstPersonController>().human && player.GetComponent<FirstPersonController>().hintBook) // book
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                }
                else if(theObject.transform.gameObject.GetComponentInChildren<Door>() != null && player.GetComponent<FirstPersonController>().human && player.GetComponent<FirstPersonController>().hintDoor) // door
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                }
                else if (theObject.transform.gameObject.GetComponentInChildren<holdable>() != null && player.GetComponent<FirstPersonController>().human && player.GetComponent<FirstPersonController>().hintKey) // keys
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                }
                else if(theObject.transform.gameObject.GetComponentInChildren<destructible>() != null && !player.GetComponent<FirstPersonController>().human && player.GetComponent<FirstPersonController>().hintDestructible) // destructible
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                }
                else if (theObject.transform.GetComponent<itemToMakePotion>() != null) // destructible
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                }


                //Le chaudron
                if (theObject.transform.name == "CHaudron_scenes" && GameManager.instance.gameState == GameManager.GameState.playing)
                {
                    theObject.transform.gameObject.GetComponent<Outline>().enabled = true;
                    if (Mouse.current.leftButton.wasPressedThisFrame)
                    {
                        GameManager.instance.createPotion.SetActive(true);
                        GameManager.instance.createPotion.GetComponent<CreatePotionUiHandler>().startUp();
                    }
                }




                if (Mouse.current.leftButton.wasPressedThisFrame)
                {
                    // book
                    if (theObject.transform.gameObject.name == "bookReadable" && GameManager.instance.gameState != GameManager.GameState.readingBook)
                    {
                        theObject.transform.gameObject.GetComponentInChildren<Canvas>().enabled = true;
                        GameManager.instance.gameState = GameManager.GameState.readingBook;
                    }
                    else if (theObject.transform.gameObject.name == "bookReadable" && GameManager.instance.gameState == GameManager.GameState.readingBook)
                    {
                        theObject.transform.gameObject.GetComponentInChildren<Canvas>().enabled = false;
                        GameManager.instance.gameState = GameManager.GameState.playing;
                    }

                    //door 
                    if (theObject.transform.gameObject.GetComponent<Door>() != null && player.GetComponent<FirstPersonController>().human) // door
                    {
                        Door door = theObject.transform.gameObject.GetComponent<Door>();
                        Debug.Log("Hello");
                        if (!door.keyNeeded)
                        {
                            player.GetComponent<AudioSource>().clip = door.openSound;
                            player.GetComponent<AudioSource>().Play();
                            door.open = !door.open;
                            Debug.Log("opened");

                        } else if(door.keyNeeded && door.keyToOpen != null && door.keyToOpen == player.GetComponent<FirstPersonController>().holdedItem)
                        {
                            door.keyNeeded = false;
                            player.GetComponent<FirstPersonController>().hold = false;
                            Destroy(player.GetComponent<FirstPersonController>().holdedItem);
                        }
                        else if (door.keyNeeded && (door.keyToOpen == null || door.keyToOpen != player.GetComponent<FirstPersonController>().holdedItem))
                        {
                            player.GetComponent<AudioSource>().clip = door.lockedSound;
                            player.GetComponent<AudioSource>().Play();
                        }

                    }

                    //holdable
                    if (theObject.transform.gameObject.GetComponent<holdable>() != null)
                    {
                        Debug.Log(player.GetComponent<FirstPersonController>().human);
                        if (player.GetComponent<FirstPersonController>().human)
                        {
                            player.GetComponent<FirstPersonController>().hold = true;
                            player.GetComponent<FirstPersonController>().holdedItem = theObject.transform.gameObject;
                            Debug.Log(theObject.transform.gameObject);
                        }
                    }

                    //item to make potion
                    if (theObject.transform.gameObject.GetComponent<itemToMakePotion>() != null)
                    {
                        GameManager.instance.itemsToMakePotion.Add(theObject.transform.gameObject.GetComponent<itemToMakePotion>());

                        UiItemsToMakePotions.instance.display(theObject.transform.gameObject.GetComponent<itemToMakePotion>());

                        theObject.transform.gameObject.SetActive(false);
                    }
                }

             
            } else if(triggered)
            {
                triggered = false;
                lastTriggered.GetComponent<Outline>().enabled = false;
            }

        }
    }
}
