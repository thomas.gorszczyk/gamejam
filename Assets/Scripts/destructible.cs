using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destructible : MonoBehaviour
{
    public AudioSource audiosource;

    public AudioClip smashClip;
    public AudioClip destroyClip;


    public float pv = 3f;
    // Update is called once per frame
    void Update()
    {
        
    }

    void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        Debug.Log(other.gameObject);
        Debug.Log(other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponentInParent<AudioSource>().clip = smashClip;
            other.gameObject.GetComponentInParent<AudioSource>().Play();
            Debug.Log(other);
            pv = pv - 1f;
            Debug.Log(pv);
        }

        if(pv <= 0f)
        {
            Debug.Log("pla ket ket");
            other.gameObject.GetComponentInParent<AudioSource>().clip = destroyClip;
            other.gameObject.GetComponentInParent<AudioSource>().Play();
            this.gameObject.SetActive(false);
        }
        

    }

}
