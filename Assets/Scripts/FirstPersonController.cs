using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	[RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
	[RequireComponent(typeof(PlayerInput))]
#endif
	public class FirstPersonController : MonoBehaviour
	{
		[Header("Player")]
        [Tooltip("pause mode")]
        public static bool pause = false;
        [Tooltip("Move speed of the character in m/s")]
		public float MoveSpeed = 4.0f;
		[Tooltip("Sprint speed of the character in m/s")]
		public float SprintSpeed = 6.0f;
		[Tooltip("Rotation speed of the character")]
		public float RotationSpeed = 1.0f;
		[Tooltip("Acceleration and deceleration")]
		public float SpeedChangeRate = 10.0f;

		[Space(10)]
		[Tooltip("The height the player can jump")]
		public float JumpHeight = 1.2f;
		[Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
		public float Gravity = -15.0f;

		[Space(10)]
		[Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
		public float JumpTimeout = 0.1f;
		[Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
		public float FallTimeout = 0.15f;

		[Space(10)]
		public bool isCrouched = false;
		public bool isProned = false;

		[Header("Player Grounded")]
		[Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
		public bool Grounded = true;
		[Tooltip("Useful for rough ground")]
		public float GroundedOffset = -0.14f;
		[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
		public float GroundedRadius = 0.5f;
		[Tooltip("What layers the character uses as ground")]
		public LayerMask GroundLayers;

		[Header("Cinemachine")]
		[Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
		public GameObject CinemachineCameraTarget;
		[Tooltip("How far in degrees can you move the camera up")]
		public float TopClamp = 90.0f;
		[Tooltip("How far in degrees can you move the camera down")]
		public float BottomClamp = -90.0f;

        [Header("Type")]
        [Tooltip("human or monster")]
        public bool human = true;
        [Tooltip("canSmash")]
        public bool canSmash = true;
        [Tooltip("smash couldown")]
        public float smashCooldown = 0f;
        [Tooltip("max smash couldown")]
        public float smashCooldownMax = 1f;
        [Tooltip("speed smash")]
        public float speedSmash = 1f;
        [Tooltip("hold")]
        public bool hold = false;
        [Tooltip("hold")]
        public GameObject holdedItem;

        [Header("Hints")]
        [Tooltip("books")]
        public bool hintBook = true;
        [Tooltip("door")]
        public bool hintDoor = false;
        [Tooltip("keys")]
        public bool hintKey = false;
        [Tooltip("destructible")]
        public bool hintDestructible = false;
        [Tooltip("pushable")]
        public bool hintPush = false;

        // cinemachine
        private float _cinemachineTargetPitch;

		// player
		private float _speed;
		private float _rotationVelocity;
		private float _verticalVelocity;
		private float _terminalVelocity = 53.0f;

		// timeout deltatime
		private float _jumpTimeoutDelta;
		private float _fallTimeoutDelta;

		private PlayerInput _playerInput;
		private CharacterController _controller;
		private StarterAssetsInputs _input;
		private GameObject _mainCamera;

		public GameObject Capsule;

		private const float _threshold = 0.01f;

		private Vector3 _defaultPlayerColliderSize = new Vector3(1, 2, 1);

		private Vector3 _defaultPlayerColliderCenter = new Vector3(1, 1, 1);

		private bool IsCurrentDeviceMouse => _playerInput.currentControlScheme == "KeyboardMouse";

		private void Awake()
		{
			// get a reference to our main camera
			if (_mainCamera == null)
			{
				_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
			}
		}

		private void Start()
		{
			_controller = GetComponent<CharacterController>();
			_input = GetComponent<StarterAssetsInputs>();
			_playerInput = GetComponent<PlayerInput>();

			// reset our timeouts on start
			_jumpTimeoutDelta = JumpTimeout;
			_fallTimeoutDelta = FallTimeout;
			
		}

		private void Update()
		{
            Model();

			if(GameManager.instance.gameState == GameManager.GameState.playing)
            {
				JumpAndGravity();
				GroundedCheck();
				Move();
				Smash();
				Hold();
				Crouch();
				Prone();
			}

            if (Keyboard.current.oKey.wasPressedThisFrame) human = !human;
        }

        private void FixedUpdate()
        {

        }

        private void LateUpdate()
		{
			if(GameManager.instance.gameState == GameManager.GameState.playing)
            {
				CameraRotation();
			}
			
		}

        private void Hold()
        {
            if(hold && holdedItem != null)
            {
                holdedItem.transform.position = gameObject.transform.GetChild(1).GetChild(0).GetChild(0).transform.position;

                if (Mouse.current.rightButton.wasPressedThisFrame)
                {
                    hold = false;
                    holdedItem = null;  
                }
            }

        }

        private void Model()
        {
            if (human && gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material.color != Color.black)
            {
                //gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = Resources.Load("Art/Materials/human.mat", typeof(Material)) as Material;
                gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material.color = Color.black;
                gameObject.transform.GetChild(1).GetChild(0).transform.localScale = new Vector3(0.1f, 0.5f, 0.15f);

            } else if (!human && gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material.color != Color.green)
            {
                Debug.Log(gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material);
                gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material.color = Color.green;
                gameObject.transform.GetChild(1).GetChild(0).transform.localScale = new Vector3(0.2f, 0.5f, 0.2f);
                //gameObject.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = Resources.Load("Art/Materials/monster.mat", typeof(Material)) as Material;
            }
            
        }

        private void GroundedCheck()
		{
			// set sphere position, with offset
			Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
			Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
		}

		private void CameraRotation()
		{
			// if there is an input
			if (_input.look.sqrMagnitude >= _threshold && !pause)
			{
				//Don't multiply mouse input by Time.deltaTime
				float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
				
				_cinemachineTargetPitch += _input.look.y * RotationSpeed * deltaTimeMultiplier;
				_rotationVelocity = _input.look.x * RotationSpeed * deltaTimeMultiplier;

				// clamp our pitch rotation
				_cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

				// Update Cinemachine camera target pitch
				CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);

				// rotate the player left and right
				transform.Rotate(Vector3.up * _rotationVelocity);
			}
		}

        Vector3 baseposition;
        Vector3 endposition;
        bool lerpForward, lerpBackward;
        private void Smash()
        {
            if(Keyboard.current.hKey.wasPressedThisFrame)
            {
                Debug.Log("test");
            }
            if (human) return;
            if (Mouse.current.rightButton.wasPressedThisFrame && canSmash)
            {

                
                baseposition = gameObject.transform.GetChild(1).GetChild(0).localPosition;
                endposition = new Vector3(gameObject.transform.GetChild(1).GetChild(0).localPosition.x, gameObject.transform.GetChild(1).GetChild(0).localPosition.y, gameObject.transform.GetChild(1).GetChild(0).localPosition.z + 1.5f);
                canSmash = false;
                lerpForward = true;
                lerpBackward = false;
            }

            if(smashCooldown > 0.6 * smashCooldownMax && gameObject.transform.GetChild(1).GetChild(0).GetComponent<BoxCollider>().enabled == false)
            {
                gameObject.transform.GetChild(1).GetChild(0).GetComponent<BoxCollider>().enabled = true;
            } else if(smashCooldown < 0.6 * smashCooldownMax && gameObject.transform.GetChild(1).GetChild(0).GetComponent<BoxCollider>().enabled == true)
            {
                gameObject.transform.GetChild(1).GetChild(0).GetComponent<BoxCollider>().enabled = false;
            }

            if (smashCooldown < smashCooldownMax && !canSmash && lerpForward)
            {
                gameObject.transform.GetChild(1).GetChild(0).localPosition = Vector3.Lerp(baseposition, endposition, smashCooldown);
                smashCooldown += Time.deltaTime;

            } else if (smashCooldown >= 0 && !canSmash)
            {
                lerpForward = false;
                lerpBackward = true;
                gameObject.transform.GetChild(1).GetChild(0).localPosition = Vector3.Lerp(baseposition, endposition, smashCooldown);
                smashCooldown -= Time.deltaTime;

            } else if (smashCooldown < 0f && !canSmash)
            {
                canSmash = !canSmash;
                smashCooldown = 0f;
                gameObject.transform.GetChild(1).GetChild(0).localPosition = baseposition;
                lerpForward = true;
                lerpBackward = false;
            }


        }

        private void Move()
		{
			// set target speed based on move speed, sprint speed and if sprint is pressed
			float targetSpeed = _input.sprint ? SprintSpeed : MoveSpeed;

			// a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

			// note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is no input, set the target speed to 0
			if (_input.move == Vector2.zero) targetSpeed = 0.0f;

			// a reference to the players current horizontal velocity
			float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

			float speedOffset = 0.1f;
			float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;

			// accelerate or decelerate to target speed
			if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
			{
				// creates curved result rather than a linear one giving a more organic speed change
				// note T in Lerp is clamped, so we don't need to clamp our speed
				_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

				// round speed to 3 decimal places
				_speed = Mathf.Round(_speed * 1000f) / 1000f;
			}
			else
			{
				_speed = targetSpeed;
			}

			// normalise input direction
			Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized;

			// note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is a move input rotate player when the player is moving
			if (_input.move != Vector2.zero)
			{
				// move
				inputDirection = transform.right * _input.move.x + transform.forward * _input.move.y;
			}

			// move the player
			_controller.Move(inputDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
		}

		private void JumpAndGravity()
		{
			if (Grounded)
			{
				// reset the fall timeout timer
				_fallTimeoutDelta = FallTimeout;

				// stop our velocity dropping infinitely when grounded
				if (_verticalVelocity < 0.0f)
				{
					_verticalVelocity = -2f;
				}

				// Jump
				if (_input.jump && _jumpTimeoutDelta <= 0.0f)
				{
					// the square root of H * -2 * G = how much velocity needed to reach desired height
					_verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);
				}

				// jump timeout
				if (_jumpTimeoutDelta >= 0.0f)
				{
					_jumpTimeoutDelta -= Time.deltaTime;
				}
			}
			else
			{
				// reset the jump timeout timer
				_jumpTimeoutDelta = JumpTimeout;

				// fall timeout
				if (_fallTimeoutDelta >= 0.0f)
				{
					_fallTimeoutDelta -= Time.deltaTime;
				}

				// if we are not grounded, do not jump
				_input.jump = false;
			}

			// apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
			if (_verticalVelocity < _terminalVelocity)
			{
				_verticalVelocity += Gravity * Time.deltaTime;
			}
		}

		private void Crouch()
        {
			if (Grounded && !isProned)
            {
				BoxCollider capsuleCollider = Capsule.GetComponent<BoxCollider>();
				if (Keyboard.current.leftCtrlKey.wasPressedThisFrame)
				{
					if (!isCrouched)
                    {
						isCrouched = true;
                        Capsule.transform.parent.transform.localScale = new Vector3(1f, 0.5f, 1f);
                        Capsule.transform.GetChild(0).transform.localScale = new Vector3(0.2f, 0.5f, 0.4f);
                        //capsuleCollider.center = new Vector3(0f, 0f, 0f);
                        //capsuleCollider.size = new Vector3(1, 1, 1);
                        //CinemachineCameraTarget.transform.Translate(new Vector3(0, -0.5f, 0));
                        MoveSpeed = 2.4f;
						return;
                    }
					isCrouched = false;
                    //capsuleCollider.center = _defaultPlayerColliderCenter;
                    //capsuleCollider.size = _defaultPlayerColliderSize;
                    //CinemachineCameraTarget.transform.Translate(new Vector3(0, 0.5f, 0));
                    Capsule.transform.parent.transform.localScale = new Vector3(1f, 1f, 1f);
                    Capsule.transform.GetChild(0).transform.localScale = new Vector3(0.2f, 0.5f, 0.2f);
                    MoveSpeed = 4.0f;

				}
			}
        }

		private void Prone()
		{
			if (Grounded && !isCrouched)
			{
				BoxCollider capsuleCollider = Capsule.GetComponent<BoxCollider>();
				if (Keyboard.current.cKey.wasPressedThisFrame)
				{
					if (!isProned)
					{
						isProned = true;
                        //capsuleCollider.center = new Vector3(0f, 1f, 0f);
                        //capsuleCollider.size = new Vector3(1, 0.3f, 1);
                        //CinemachineCameraTarget.transform.Translate(new Vector3(0, -1, 0));
                        Capsule.transform.parent.transform.localScale = new Vector3(1f, 0.25f, 1f);
                        Capsule.transform.GetChild(0).transform.localScale = new Vector3(0.2f, 0.5f, 0.8f);
                        MoveSpeed = 1.3f;
						return;
					}
					isProned = false;
                    //capsuleCollider.center = _defaultPlayerColliderCenter;
                    //capsuleCollider.size = _defaultPlayerColliderSize;
                    //CinemachineCameraTarget.transform.Translate(new Vector3(0, 1, 0));
                    Capsule.transform.parent.transform.localScale = new Vector3(1f, 1f, 1f);
                    Capsule.transform.GetChild(0).transform.localScale = new Vector3(0.2f, 0.5f, 0.2f);
                    MoveSpeed = 4.0f;

				}
			}
		}


		private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
		{
			if (lfAngle < -360f) lfAngle += 360f;
			if (lfAngle > 360f) lfAngle -= 360f;
			return Mathf.Clamp(lfAngle, lfMin, lfMax);
		}

		private void OnDrawGizmosSelected()
		{
			Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
			Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

			if (Grounded) Gizmos.color = transparentGreen;
			else Gizmos.color = transparentRed;

			// when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
			Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z), GroundedRadius);
		}
	}
}