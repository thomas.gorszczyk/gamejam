using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StarterAssets;

public class WaterTrigger : MonoBehaviour
{
    int userTimeInWater = 0;

    const int maxTimeUserInWater = 5;

    bool isUserInWater = false;

    public GameObject warningText;

    protected float Timer;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isUserInWater)
        {
            Timer += Time.deltaTime;

            if (Timer >= 1)
            {
                userTimeInWater += 1;
                Timer = 0f;
            }

            if (userTimeInWater == maxTimeUserInWater)
            {
                killUser();
                resetAll();
            } else
            {
                warningText.SetActive(true);
                warningText.GetComponent<Text>().text = "Vous allez mourir dans " + (maxTimeUserInWater - userTimeInWater) + " secondes si vous ne sortez pas de l'eau";
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        isUserInWater = true;
        
    }

    void OnTriggerExit(Collider other)
    {
        this.resetAll();
    }

    private void killUser()
    {
        GameManager.instance.gameState = GameManager.GameState.gameOver;
    }

    private void resetAll()
    {
        isUserInWater = false;
        userTimeInWater = 0;
        warningText.SetActive(false);
    }
}
