using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAvailableForPotion : MonoBehaviour
{
    public itemToMakePotion itemToMakePotion;
    public Image icon;
   
    public void startUp()
    {
        icon.sprite = itemToMakePotion.icon;

    }

    // Update is called once per frame
    void Update()
    {
        if(CreatePotionUiHandler.instance.numberOfSelectedItem == 3)
        {
            CreatePotionUiHandler.instance.makeItem.interactable = true;
        } 
        else
        {
            CreatePotionUiHandler.instance.makeItem.interactable = false;
        }
    }

    public void click()
    { 

        if(transform.parent.name == "whereToSpawnSelectedItem")
        {
            transform.parent = CreatePotionUiHandler.instance.whereToSpawnAvailableItem.transform;
            CreatePotionUiHandler.instance.numberOfSelectedItem--;
        } 
        else if (transform.parent.name == "whereToSpawnAvailableItem")
        {
            transform.parent = CreatePotionUiHandler.instance.whereToSpawnSelectedItem.transform;
            CreatePotionUiHandler.instance.numberOfSelectedItem++;
        }

        
    }

}
