using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiDayNight : MonoBehaviour
{
    public Text displayDayOrNight;
    public Image imageTimer;
    public GameObject dayTimeDisplay;
    void Start()
    {
        //displayDayOrNight.text = "ezloefgneroijgfrie";
    }

    // Update is called once per frame
    void Update()
    {
        /*if(GameManager.instance.gameState == GameManager.GameState.dayTransition)
        {
            displayDayOrNight.text = GameManager.GameState.dayTransition + "";
        }
        /*else
        {
            displayDayOrNight.text = GameManager.instance.timeStep[GameManager.instance.dayOrNightStepNumber].dayOrNight + "";

        }*/

        // elapsedTime = GameManager.instance.


        //imageTimer.fillAmount = GameManager.instance.timeSinceLastSteps * 100 / GameManager.instance.timeStep[GameManager.instance.dayOrNightStepNumber].time;

        if (GameManager.instance.gameState == GameManager.GameState.playing)
        {
            if (!dayTimeDisplay.activeSelf)
            {
                dayTimeDisplay.SetActive(true);
            }

            imageTimer.fillAmount = GameManager.instance.timeSinceLastSteps * 100 / GameManager.instance.timeStep[GameManager.instance.dayOrNightStepNumber].time / 100;

        } 
        else
        {
            if (dayTimeDisplay.activeSelf)
            {
                dayTimeDisplay.SetActive(false);
            }
        }
      
    }
}
