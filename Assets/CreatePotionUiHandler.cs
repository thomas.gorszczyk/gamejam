using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatePotionUiHandler : MonoBehaviour
{
    public GameObject itemAvailableForPotionPrefab;

    public GameObject whereToSpawnAvailableItem;
    public GameObject whereToSpawnSelectedItem;

    public static CreatePotionUiHandler instance;

    public int numberOfSelectedItem = 0;

    public Button makeItem;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }

    }

    public void startUp()
    {

        GameManager.instance.gameState = GameManager.GameState.potionUI;

        Cursor.lockState = CursorLockMode.None;

        foreach (Transform child in whereToSpawnAvailableItem.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in whereToSpawnSelectedItem.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

            

        foreach (var item in GameManager.instance.itemsToMakePotion)
        {
            GameObject uItem = Instantiate(itemAvailableForPotionPrefab, transform.position, transform.rotation) as GameObject;
            uItem.transform.parent = whereToSpawnAvailableItem.transform;

            uItem.GetComponent<ItemAvailableForPotion>().itemToMakePotion = item;
            uItem.GetComponent<ItemAvailableForPotion>().startUp();
        }
    }

    public void makePotion()
    {
        int numberOfWinningItem = 0;
        foreach(Transform child in whereToSpawnSelectedItem.transform)
        {
            if (child.GetComponent<ItemAvailableForPotion>().itemToMakePotion.isWinningItem)
            {
                numberOfWinningItem++;
            }
        }

        GameManager.instance.gameState = GameManager.GameState.victory;
    
    }

    public void exit()
    {
        transform.gameObject.SetActive(false);
        GameManager.instance.gameState = GameManager.GameState.playing;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
