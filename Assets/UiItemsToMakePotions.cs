using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiItemsToMakePotions : MonoBehaviour
{
    public static UiItemsToMakePotions instance;

    public GameObject UItem;

    public GameObject panelForUItem;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

    public void Start()
    {
        foreach (var item in GameManager.instance.itemsToMakePotion)
        {
            display(item);
        }
    }

    public void display(itemToMakePotion item)
    {
        GameObject uItem = Instantiate(UItem, transform.position, transform.rotation) as GameObject;
        uItem.transform.parent = panelForUItem.transform;
        uItem.GetComponent<Image>().sprite = item.icon;

    }
}
