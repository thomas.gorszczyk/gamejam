using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorCabane : MonoBehaviour
{
    public GameObject doorToOpen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter()
    {
        if (doorToOpen)
        {
            doorToOpen.GetComponent<Door>().keyNeeded = false;
            doorToOpen.GetComponent<Door>().open = true;
        }
    }
}
